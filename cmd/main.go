package main

import (
	"github.com/sirupsen/logrus"
	"github.com/spf13/pflag"
	"gitlab.com/leboncoin1/fizz-buzz-api/pkg/server"
)

func main() {
	//parse flags
	pflag.Parse()
	//run server
	server.Run(logrus.StandardLogger())
}
