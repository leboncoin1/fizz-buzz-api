package domain

// WrongArgumentTypeError is used to detect when the type of argument in fizzbuzz request is wrong
var WrongArgumentTypeError error = wrongArgumentTypeError{}

type wrongArgumentTypeError struct{}

func (e wrongArgumentTypeError) Error() string {
	return "wrong argument type"
}

// InternalError is used to detect when the calculate gets wrong
var InternalError error = internalError{}

type internalError struct{}

func (e internalError) Error() string {
	return "internal error"
}

// MethodNotFound is used to detect a method not implemented
var MethodNotFound error = methodNotFoundError{}

type methodNotFoundError struct{}

func (e methodNotFoundError) Error() string {
	return "method not found"
}
