package domain

import (
	"fmt"

	"github.com/spf13/pflag"
)

const (
	ConstDefaultEnvironment = "preprod"
	Calculate               = "calculate"
)

var (
	// FlagEnvironment flag
	FlagEnvironment = pflag.String("environment", ConstDefaultEnvironment, "Environment string. This is optional.")
)

// FizzBuzzRequest represent a fizzbuzz request
type FizzBuzzRequest struct {
	Int1    int64
	Int2    int64
	Limit   int64
	String1 string
	String2 string
	Action  string
}

//FizzBuzzCounter represent a fizzbuzz element for stats
type FizzBuzzCounter struct {
	Query   FizzBuzzRequest
	Counter int64
}

func (fzc *FizzBuzzCounter) String() string {
	return fmt.Sprintf("string1: %s, string2: %s, n1: %d, n2: %d, limit: %d. Counter: %d",
		fzc.Query.String1, fzc.Query.String2, fzc.Query.Int1, fzc.Query.Int2, fzc.Query.Limit, fzc.Counter)
}
