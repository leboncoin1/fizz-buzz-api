package handlers

import (
	"encoding/json"
	"net/http"

	"github.com/sirupsen/logrus"
)

type healthHandler struct {
	logger logrus.FieldLogger
}

//NewHealthHandler create new health Handler
func NewHealthHandler(logger logrus.FieldLogger) http.Handler {
	return &healthHandler{
		logger: logger,
	}
}

func (h *healthHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")

	w.WriteHeader(http.StatusOK)

	resp := fizzBuzzResponse{
		Status:  "200",
		Message: "OK",
	}

	err := json.NewEncoder(w).Encode(resp)
	if err != nil {
		h.logger.Errorf("writeResponse(): %v", err)
	}
}
