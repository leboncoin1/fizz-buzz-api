//package handlers contains methods to /post fizzbuzz route to replace the multiples of int1 by string1
//and multiples of int2 by string2, the common multiples are replaced by the merge of two strings passed in json.
//this package expose also /health to verify the if server is running
//	`{
// 		"int1": "3",
// 		"int2": "5",
// 		"limit": "20",
// 		"string1": "toto",
// 		"string2": "tata"
// 	}`
package handlers

import (
	"context"
	"encoding/json"
	"errors"
	"math"
	"net/http"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/leboncoin1/fizz-buzz-api/pkg/domain"
	"gitlab.com/leboncoin1/fizz-buzz-api/pkg/fizzbuzz"
)

const (
	success         = "success"
	failed          = "failed"
	zero            = "0"
	LimitMax        = 1000000
	LimitDefault    = 10
	timeOutDuration = 20 * time.Second
)

type fizzBuzzRequest struct {
	Int1    json.Number `json:"int1"`
	Int2    json.Number `json:"int2"`
	Limit   json.Number `json:"limit"`
	String1 string      `json:"string1"`
	String2 string      `json:"string2"`
}

type fizzBuzzResponse struct {
	Status  string   `json:"status"`
	Message string   `json:"message"`
	Numbers []string `json:"numbers,omitempty"`
}

type fizzBuzzHandler struct {
	logger logrus.FieldLogger
	fzbz   fizzbuzz.FizzBuzz
	hits   map[string]*domain.FizzBuzzCounter
}

//NewFizzBuzzHandler create new fizzBuzzHandler
func NewFizzBuzzHandler(ctx context.Context, close chan struct{},
	hitsStats map[string]*domain.FizzBuzzCounter,
	logger logrus.FieldLogger) (http.Handler, error) {

	h := &fizzBuzzHandler{
		logger: logger,
		fzbz:   fizzbuzz.NewFizzBuzz(logger),
		hits:   hitsStats,
	}

	go h.waitToClose(ctx, close)
	//return handler and no errors
	return h, nil
}

func (h *fizzBuzzHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var fbzRequest fizzBuzzRequest
	// launch timeout
	ctx, cancelFunc := context.WithTimeout(context.Background(), timeOutDuration)
	defer cancelFunc()

	// start decoding request
	err := json.NewDecoder(r.Body).Decode(&fbzRequest)
	if err != nil {
		h.logger.Errorf("json.Decode(): %v", err)
		h.writeResponse(w, err.Error(), []string{}, http.StatusBadRequest)

		return
	}

	request, err := h.convertRequest(fbzRequest, r.Method)
	if err != nil {
		h.handleRequestError(w, err)
		return
	}

	err = h.checkArguments(request)
	if err != nil {
		h.handleRequestError(w, err)
		return
	}
	//TODO: set stats here
	err = h.checkRequest(request)
	if err != nil {
		h.handleRequestError(w, err)
		return
	}
	err = h.handleCompute(ctx, w, request)
	if err != nil {
		h.logger.Errorf("handle %s error: %v", request.Action, err)
		h.handleResponseError(w, err)
	}
}

func (h *fizzBuzzHandler) handleResponseError(w http.ResponseWriter, err error) {
	errMsg := err.Error()
	status := http.StatusBadRequest

	if errors.Is(err, context.DeadlineExceeded) {
		status = http.StatusServiceUnavailable
		errMsg = "service temporarily unavailable"
	} else if errors.Is(err, domain.InternalError) {
		status = http.StatusInternalServerError
		errMsg = domain.InternalError.Error()
	}

	h.writeResponse(w, errMsg, []string{}, status)
}

func (h *fizzBuzzHandler) handleCompute(ctx context.Context, w http.ResponseWriter, r *domain.FizzBuzzRequest) error {
	h.logger.Infof("FizzBuzz request received: %v", r)

	elements, err := h.fzbz.Calculate(ctx, r.String1, r.String2, r.Int1, r.Int2, r.Limit)
	if err != nil {
		return domain.InternalError
	}
	msg := "OK"
	h.writeResponse(w, msg, elements, http.StatusOK)
	//no errors
	return nil
}

func (h *fizzBuzzHandler) convertRequest(r fizzBuzzRequest, method string) (*domain.FizzBuzzRequest, error) {
	var action string
	var err error

	switch method {
	case http.MethodPost:
		action = domain.Calculate
	default:
		return nil, domain.MethodNotFound
	}

	int1, err := r.Int1.Int64()
	if err != nil {
		return nil, errors.New("wrong int1 argument/missing")
	}
	int2, err := r.Int2.Int64()
	if err != nil {
		return nil, errors.New("wrong int2 argument/missing")
	}
	limit, err := r.Limit.Int64()
	if err != nil {
		return nil, errors.New("wrong limint argument/missing")
	}
	//return new instance of FizzBuzzRequest
	return &domain.FizzBuzzRequest{
		Int1:    int1,
		Int2:    int2,
		Limit:   limit,
		String1: r.String1,
		String2: r.String2,
		Action:  action,
	}, nil
}

func (h *fizzBuzzHandler) checkArguments(r *domain.FizzBuzzRequest) error {
	if len(r.String1) == 0 {
		return errors.New("string1 empty")
	} else if len(r.String2) == 0 {
		return errors.New("string2 empty")
	} else if math.Abs(float64(r.Limit)) > float64(LimitMax) {
		return errors.New("limit max overpassed")
	} else if r.Int1 == 0 {
		return errors.New("not divide by zero")
	} else if r.Int2 == 0 {
		return errors.New("not divide by zero")
	}
	//set limit default
	if r.Limit == 0 {
		r.Limit = LimitDefault
	}

	return nil
}

func (h *fizzBuzzHandler) handleRequestError(w http.ResponseWriter, err error) {
	status := http.StatusBadRequest
	if errors.Is(err, domain.InternalError) {
		status = http.StatusInternalServerError
	} else if errors.Is(err, domain.MethodNotFound) {
		status = http.StatusNotFound
	}

	h.writeResponse(w, err.Error(), []string{}, status)
}

func (h *fizzBuzzHandler) writeResponse(w http.ResponseWriter, msg string, numbers []string, httpStatus int) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")

	statusStr := success

	if httpStatus != http.StatusOK {
		statusStr = failed
	}

	w.WriteHeader(httpStatus)

	resp := fizzBuzzResponse{
		Status:  statusStr,
		Message: msg,
		Numbers: numbers,
	}

	err := json.NewEncoder(w).Encode(resp)
	if err != nil {
		h.logger.Errorf("writeResponse(): %v", err)
	}
}

func (h *fizzBuzzHandler) waitToClose(ctx context.Context, close chan struct{}) {
	for {
		select {
		case <-ctx.Done():
			//close here any process from handler
			close <- struct{}{}
			return
		default:
		}
	}
}

func (h *fizzBuzzHandler) checkRequest(r *domain.FizzBuzzRequest) error {
	hashQuery, err := h.fzbz.ToMd5(r.String1, r.String2, r.Int1, r.Int2, r.Limit)
	if err != nil {
		return domain.InternalError
	}
	if q, ok := h.hits[hashQuery]; ok {
		q.Counter++
	} else {
		h.hits[hashQuery] = &domain.FizzBuzzCounter{Query: *r, Counter: 1}
	}
	//TODO: add stats here
	return nil
}
