package handlers

import (
	"encoding/json"
	"net/http"
	"sort"

	"github.com/sirupsen/logrus"
	"gitlab.com/leboncoin1/fizz-buzz-api/pkg/domain"
)

type statsHandler struct {
	logger logrus.FieldLogger
	hits   map[string]*domain.FizzBuzzCounter
}

type fizzBuzzStatResponse struct {
	Status  string   `json:"status"`
	Message string   `json:"message"`
	Queries []string `json:"queries"`
}

//NewStatsHandler create new stats Handler
func NewStatsHandler(hitsStats map[string]*domain.FizzBuzzCounter, logger logrus.FieldLogger) http.Handler {
	return &statsHandler{
		logger: logger,
		hits:   hitsStats,
	}
}

func (h *statsHandler) getStats() []string {
	sortedHits := make([]*domain.FizzBuzzCounter, 0, len(h.hits))
	for _, hit := range h.hits {
		sortedHits = append(sortedHits, hit)
	}
	//sort hits
	sort.SliceStable(sortedHits, func(i, j int) bool {
		return sortedHits[i].Counter > sortedHits[j].Counter
	})

	list := make([]string, 0, len(h.hits))

	for _, hit := range sortedHits {
		list = append(list, hit.String())
	}
	//return sorted list
	return list
}

func (h *statsHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		h.writeResponse(w, "OK", h.getStats(), http.StatusOK)
	default:
		//  domain.MethodNotFound
		h.writeResponse(w, "Method not found", []string{}, http.StatusBadRequest)
	}
}

func (h *statsHandler) writeResponse(w http.ResponseWriter, msg string, queries []string, httpStatus int) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")

	statusStr := success

	if httpStatus != http.StatusOK {
		statusStr = failed
	}

	w.WriteHeader(httpStatus)

	resp := fizzBuzzStatResponse{
		Status:  statusStr,
		Message: msg,
		Queries: queries,
	}

	err := json.NewEncoder(w).Encode(resp)
	if err != nil {
		h.logger.Errorf("writeResponse(): %v", err)
	}
}
