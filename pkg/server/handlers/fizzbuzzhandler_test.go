package handlers

import (
	"bytes"
	"context"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"testing"

	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/leboncoin1/fizz-buzz-api/pkg/fizzbuzz/fizzbuzzmock"
)

// httpWriteMock
type httpWriterMock struct {
	buffer []byte
	status int
}

func (h *httpWriterMock) Header() http.Header {
	return map[string][]string{}
}

func (h *httpWriterMock) Write(b []byte) (int, error) {
	h.buffer = append(h.buffer, b...)
	return len(b), nil
}

func (h *httpWriterMock) WriteHeader(statusCode int) {
	h.status = statusCode
}

// Test Cases
type testCase struct {
	label          string
	requestStr     string
	method         string
	expectedStatus int
}

func getCases() []*testCase {
	return []*testCase{
		{
			label: "POST request",
			requestStr: `{
				"int1": "3",
				"int2": "5",
				"limit": "20",
				"string1": "toto",
				"string2": "tata"
			}`,
			method:         "POST",
			expectedStatus: 200,
		},
		{
			label: "POST request",
			requestStr: `{
				"int1": "3",
				"int2": "5",
				"limit": "100",
				"string1": "toto",
				"string2": "tata"
			}`,
			method:         "POST",
			expectedStatus: 500,
		},
		{
			label: "missing int1",
			requestStr: `{
				"int2": "5",
				"limit": "20",
				"string1": "toto",
				"string2": "tata"
			}`,
			method:         "POST",
			expectedStatus: 400,
		},
		{
			label: "missing int2",
			requestStr: `{
				"int1": "5",
				"limit": "20",
				"string1": "toto",
				"string2": "tata"
			}`,
			method:         "POST",
			expectedStatus: 400,
		},
		{
			label: "wrong string1",
			requestStr: `{
				"int1": "5",
				"int2": "5",
				"string1": 3,
				"string2": "tata"
			}`,
			method:         "POST",
			expectedStatus: 400,
		},
		{
			label: "string1 missing",
			requestStr: `{
				"int1": "3",
				"int2": "5",
				"limit": "10",
				"string1": "",
				"string2": "tata"
			}`,
			method:         "POST",
			expectedStatus: 400,
		},
		{
			label: "string2 missing",
			requestStr: `{
				"int1": "2",
				"int2": "5",
				"limit": "10",
				"string1": "toto",
				"string2": ""
			}`,
			method:         "POST",
			expectedStatus: 400,
		},
		{
			label: "missing string2",
			requestStr: `{
				"int1": "5",
				"int2": "5",
				"string1": "3",
			}`,
			method:         "POST",
			expectedStatus: 400,
		},
		{
			label: "Limit overpass",
			requestStr: `{
				"int1": "3",
				"int2": "5",
				"limit": "2000000",
				"string1": "toto",
				"string2": "tata"
			}`,
			method:         "POST",
			expectedStatus: 400,
		},
		{
			label: "not body overpass",
			requestStr: `{
			}`,
			method:         "POST",
			expectedStatus: 400,
		},
		{
			label:          "Wrong method",
			requestStr:     `{}`,
			method:         "GET",
			expectedStatus: 404,
		},
	}
}

func TestFizzBuzzHandler(t *testing.T) {
	logger := logrus.New()
	logger.SetOutput(ioutil.Discard)
	mockfizzbuzz := &fizzbuzzmock.FizzBuzz{}

	mockfizzbuzz.On("Calculate", mock.AnythingOfType("*context.timerCtx"), "toto", "tata", int64(3),
		int64(5), int64(20)).
		Return([]string{"1", "2", "toto", "4", "tata", "toto", "7", "8", "toto", "tata", "11", "toto",
			"13", "14", "tototata", "16", "17", "toto", "19", "tototata"}, nil)
	mockfizzbuzz.On("Calculate", mock.AnythingOfType("*context.timerCtx"), "toto", "tata", int64(3),
		int64(5), int64(100)).
		Return(nil, context.DeadlineExceeded)
	handler := &fizzBuzzHandler{logger: logger, fzbz: mockfizzbuzz}

	cases := getCases()

	for _, c := range cases {
		cc := c
		t.Run(cc.label, func(tt *testing.T) {
			w := &httpWriterMock{status: 200}
			jsonBytes := []byte(cc.requestStr)

			url := "fizzbuzz.leboncoin.com"
			req, err := http.NewRequest(cc.method, url, bytes.NewBuffer(jsonBytes))
			assert.NoError(t, err)

			handler.ServeHTTP(w, req)

			var resp fizzBuzzResponse
			json.Unmarshal(w.buffer, &resp)

			assert.Equal(tt, cc.expectedStatus, w.status, resp.Message)
		})
	}
}
