package server

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"path/filepath"
	"strings"
	"syscall"
	"time"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	"gitlab.com/leboncoin1/fizz-buzz-api/pkg/domain"
	"gitlab.com/leboncoin1/fizz-buzz-api/pkg/server/handlers"
)

const (
	shutdownTimeout = 10 * time.Second
	BufferSize      = 1000
)

var (
	flagConfigPath = pflag.StringP("confdir", "c", "./config",
		"The configuration directory. It should contain preprod and prod subdirs")
	flagPortPprof = pflag.String("pprof-port", "9377", "Port for pprof server (default 9377)")
)

//HTTPConfig REST API
type HTTPConfig struct {
	Port         string        `mapstructure:"port"`
	Readtimeout  time.Duration `mapstructure:"readtimeout"`
	Writetimeout time.Duration `mapstructure:"writetimeout"`
}

func launchPProf(standardLogger logrus.FieldLogger) {
	go func() {
		standardLogger.Infof("launch pprof on port: %s", *flagPortPprof)

		err := http.ListenAndServe(":"+*flagPortPprof, nil)
		if err != nil {
			standardLogger.Error(err)
		}
	}()
}

func signalHandler(standardLogger logrus.FieldLogger, ctxCancel context.CancelFunc,
	close chan struct{}, httpServer *http.Server) {
	var (
		sig     os.Signal
		closing bool
		timeout <-chan time.Time
	)

	c := make(chan os.Signal, 1)
	signal.Notify(c, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)

	for {
		select {
		case sig = <-c:
			if sig == syscall.SIGTERM || sig == syscall.SIGINT || sig == syscall.SIGQUIT {
				if closing {
					standardLogger.Warnf("Caught sig again: %v. Shutting down NOW.\n", sig)
					return
				}

				closing = true

				ctx, cancel := context.WithTimeout(context.Background(), shutdownTimeout)
				defer cancel()
				standardLogger.Warnf("Received signal %v, gracefully shutdown server", sig)

				if err := httpServer.Shutdown(ctx); err != nil {
					standardLogger.Error(fmt.Errorf("http server shutdown: %v", err))
				}

				standardLogger.Warn("Waiting for resources to close with ", shutdownTimeout, " timeout...")
				timeout = time.After(shutdownTimeout)
				// Cancel context. All goroutines should now shut down.
				ctxCancel()
			}
		case <-close:
			standardLogger.Warn("Close is finished, exit program")
			return
		case <-timeout:
			// timeout after shutdownTimeout seconds waiting for app to finish,
			// our application should Exit(1)
			standardLogger.Warn("Shutdown timeout after ", shutdownTimeout, ". Forcing exit...")
			return
		}
	}
}

func readConfig(configPath string) (*HTTPConfig, error) {
	v := viper.New()
	v.SetConfigFile(configPath + "/restapi.yaml")

	err := v.ReadInConfig()
	if err != nil {
		return nil, errors.Wrap(err, "config.ReadInConfig()")
	}

	config := new(HTTPConfig)

	err = v.Unmarshal(&config)
	if err != nil {
		return nil, errors.Wrap(err, "config.Unmarshal()")
	}

	return config, nil
}

//Run : start the server
func Run(standardLogger logrus.FieldLogger) error {

	close := make(chan struct{}, 1)

	// Signal
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// pprof
	launchPProf(standardLogger)

	configPath := filepath.Clean(*flagConfigPath) + "/" + *domain.FlagEnvironment

	config, err := readConfig(configPath)
	if err != nil {
		return err
	}

	hitsStats := make(map[string]*domain.FizzBuzzCounter, BufferSize)
	handler, err := handlers.NewFizzBuzzHandler(ctx, close, hitsStats, standardLogger)
	if err != nil {
		return err
	}

	health := handlers.NewHealthHandler(standardLogger)

	stats := handlers.NewStatsHandler(hitsStats, standardLogger)

	mux := http.NewServeMux()

	mux.Handle("/fizzbuzz", corsMiddleware(checkMethod(handler)))
	mux.Handle("/stats", corsMiddleware(checkMethod(stats)))
	mux.Handle("/health", corsMiddleware(checkHealthMethod(health)))

	httpServer := &http.Server{
		Addr:         ":" + config.Port,
		Handler:      mux,
		ReadTimeout:  config.Readtimeout,
		WriteTimeout: config.Writetimeout,
	}

	// Start server
	standardLogger.Infof("server run on port: %s", config.Port)

	go func() {
		if err := httpServer.ListenAndServe(); err != nil {
			if !errors.Is(err, http.ErrServerClosed) {
				standardLogger.Errorf("failed to start httpServer: %v", err)
				cancel()
			}
		}
	}()

	signalHandler(standardLogger, cancel, close, httpServer)

	return nil
}

func corsMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.Method == http.MethodOptions && r.Header.Get("Access-Control-Request-Method") != "" {
			origin := r.Header.Get("Origin")
			if origin == "" {
				origin = "*"
			}
			allowedMethods := []string{http.MethodPost}
			w.Header().Set("Access-Control-Allow-Origin", origin)
			w.Header().Set("Access-Control-Allow-Methods", strings.Join(allowedMethods, ","))
		} else {
			next.ServeHTTP(w, r)
		}
	})
}

func checkMethod(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		switch r.Method {
		case http.MethodPost, http.MethodGet:
			next.ServeHTTP(w, r)
		default:
			http.NotFound(w, r)
		}
	})
}

func checkHealthMethod(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		switch r.Method {
		case http.MethodGet:
			next.ServeHTTP(w, r)
		default:
			http.NotFound(w, r)
		}
	})
}
