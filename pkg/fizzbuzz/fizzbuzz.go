//package fizzbuzz Returns a list of strings with numbers from 1 to limit
//where: all multiples of n1 are replaced by s1,
//all multiples of n2 are replaced by s2,
//all multiples of n1 and n2 are replaced by s1s2.
package fizzbuzz

import (
	"context"
	"crypto/md5"
	"encoding/binary"
	"fmt"
	"math"
	"strconv"

	"github.com/sirupsen/logrus"
)

//interface to implement FizzBuzz Calculate
type FizzBuzz interface {
	Calculate(ctx context.Context, s1, s2 string, n1, n2, limit int64) ([]string, error)
	ToMd5(s1, s2 string, n1, n2, limit int64) (string, error)
}

type fizzbuzz struct {
	logger logrus.FieldLogger
}

func NewFizzBuzz(l logrus.FieldLogger) FizzBuzz {
	return &fizzbuzz{logger: l}
}

func getValue(n int64, s1, s2 string, n1, n2 int64) string {
	strMerged := s1 + s2
	if n%n1 == 0 && n%n2 == 0 {
		return strMerged
	} else if n%n1 == 0 {
		return s1
	} else if n%n2 == 0 {
		return s2
	}
	//return same value
	return strconv.Itoa(int(n))
}

func (fz *fizzbuzz) Calculate(ctx context.Context, s1, s2 string, n1, n2, limit int64) ([]string, error) {
	select {
	case <-ctx.Done():
		fz.logger.Error(ctx.Err(), "context done")
		return nil, ctx.Err()
	default:
		output := make([]string, 0, uint64(math.Abs(float64(limit))))
		if limit > 0 {
			for i := int64(1); i <= limit; i++ {
				output = append(output, getValue(i, s1, s2, n1, n2))
			}
		} else {
			for i := int64(1); i >= limit; i-- {
				output = append(output, getValue(i, s1, s2, n1, n2))
			}
		}
		//return list
		return output, nil
	}
}

func intToBytes(num int64) []byte {
	buf := make([]byte, binary.MaxVarintLen64)
	binary.BigEndian.PutUint64(buf, uint64(num))
	return buf
}

//ToMd5 has parameters to MD5
func (fz *fizzbuzz) ToMd5(s1, s2 string, n1, n2, limit int64) (string, error) {
	h := md5.New()
	data := []byte{}
	data = append(data, []byte(s1)...)
	data = append(data, []byte(s2)...)
	data = append(data, intToBytes(n1)...)
	data = append(data, intToBytes(n2)...)
	data = append(data, intToBytes(limit)...)
	_, err := h.Write(data)
	if err != nil {
		return "", err
	}
	return fmt.Sprintf("%x", h.Sum(nil)), nil
}
