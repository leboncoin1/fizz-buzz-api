package fizzbuzz

import (
	"context"
	"io/ioutil"
	"testing"

	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
)

// Test Cases
type testCase struct {
	label        string
	n1           int64
	n2           int64
	limit        int64
	s1           string
	s2           string
	expectedMd5  string
	expectedList []string
}

func getCases() []*testCase {
	return []*testCase{
		{
			label:       "OK",
			n1:          3,
			n2:          -5,
			limit:       20,
			s1:          "toto",
			s2:          "tata",
			expectedMd5: "4b58e133fb667bf39ad00f49387d1735",
			expectedList: []string{
				"1",
				"2",
				"toto",
				"4",
				"tata",
				"toto",
				"7",
				"8",
				"toto",
				"tata",
				"11",
				"toto",
				"13",
				"14",
				"tototata",
				"16",
				"17",
				"toto",
				"19",
				"tata",
			},
		},
		{
			label:       "OK",
			n1:          3,
			n2:          -5,
			limit:       -20,
			s1:          "toto",
			s2:          "tata",
			expectedMd5: "9d07606904ca43801b6e6e6e8c2d046b",
			expectedList: []string{
				"1",
				"tototata",
				"-1",
				"-2",
				"toto",
				"-4",
				"tata",
				"toto",
				"-7",
				"-8",
				"toto",
				"tata",
				"-11",
				"toto",
				"-13",
				"-14",
				"tototata",
				"-16",
				"-17",
				"toto",
				"-19",
				"tata",
			},
		},
	}
}

func TestFizzBuzz(t *testing.T) {
	logger := logrus.New()
	logger.SetOutput(ioutil.Discard)
	fz := &fizzbuzz{logger}

	cases := getCases()

	for _, c := range cases {
		cc := c
		t.Run(cc.label, func(tt *testing.T) {
			l, err := fz.Calculate(context.Background(), cc.s1, cc.s2, cc.n1, cc.n2, cc.limit)
			assert.NoError(t, err)
			assert.Equal(tt, cc.expectedList, l)
			hash, err := fz.ToMd5(cc.s1, cc.s2, cc.n1, cc.n2, cc.limit)
			assert.NoError(t, err)
			assert.Equal(tt, cc.expectedMd5, hash)
		})
	}
}
