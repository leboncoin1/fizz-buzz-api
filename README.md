# FIZZ - BUZZ - API

FIZZ-BUZZ-API RESTful provides services to execute the following features:

    * Calculate multiples of two integers to a limit passed as parameters.
    Note: limit max of elements 1M.
    * Stats to get the frequent request with the number of hits made

## build &  run
```
docker build -t fizz-buzz-api  .
```
```
docker run -p 9008:9008 -it fizz-buzz-api
```
## flags

**Required**

``` cmd
--environment [string - include the environment[preprod | prod]]
--confdir -c [string - include the DIR PATH to configuration files. default is ./config]
```

## Before to launch

> **preprod environnement:**

You have to set the right parameters in the restapi.yaml

``` yaml
host: ""
port: "9008"
readtimeout: 10s
writetimeout: 10s
```

## Routes

|METHOD          |ROUTE                          |MODELS                |TYPE                |
|----------------|-------------------------------|----------------------|--------------------|
|POST            |`/fizzbuzz`                    |fizzBuzzRequest       |json                |
|GET             |`/stats`                       |                      |json                |
|GET             |`/health`                      |                      |json                |

## Models

### Extract & Remove
#### Parameters:

Body: 
	```{
 		"int1": "3",
 		"int2": "5",
 		"limit": "20",
 		"string1": "toto",
 		"string2": "tata"
 	}```


##### Definition

***all parameters are required***

# fizzbuzz
```
int1: is an integer
int2: is an integer
string1: is a string
string2: is a string
```
example:
``` json
{
 		"int1": "3",
 		"int2": "5",
 		"limit": "20",
 		"string1": "toto",
 		"string2": "tata"
 	}
```

**parameter content type:** application/json

``` cmd
curl -X POST "http://localhost:9008/fizzbuzz" -H "accept: application/json" -H "Content-Type: application/json" -i -d "{
     \"int1\": 3,
     \"int2\": 5,
     \"limit\": 100,
     \"string1\": \"toto\",
     \"string2\": \"toto\"
     }"
```

``` go
 // 200: OK
 // 400: BadRequest
 // 500: Internal server error
type fizzBuzzRequest struct {
	Int1    json.Number `json:"int1"`
	Int2    json.Number `json:"int2"`
	Limit   json.Number `json:"limit"`
	String1 string      `json:"string1"`
	String2 string      `json:"string2"`
}
```


## Tree

``` tree
.
├── cmd
│   └── main.go
├── config
│   ├── preprod
│   │   └── restapi.yaml
│   └── prod
│       └── restapi.yaml
├── Dockerfile
├── go.mod
├── go.sum
├── pkg
│   ├── domain
│   │   ├── domain.go
│   │   ├── errors.go
│   │   └── README.md
│   ├── fizzbuzz
│   │   ├── fizzbuzz.go
│   │   ├── fizzbuzzmock
│   │   │   └── fizzbuzzmock.go
│   │   ├── fizzbuzz_test.go
│   │   └── README.md
│   └── server
│       ├── handlers
│       │   ├── fizzbuzzhandler.go
│       │   ├── fizzbuzzhandler_test.go
│       │   ├── health.go
│       │   ├── README.md
│       │   └── stats.go
│       ├── README.md
│       └── server.go
├── README.md

```
