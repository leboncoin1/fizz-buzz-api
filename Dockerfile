FROM golang:1.13-stretch
RUN mkdir /fizzbuzz
## /fizzbuzz directory
ADD . /fizzbuzz
WORKDIR /fizzbuzz
RUN go build -o main ./cmd/main.go
CMD ["/fizzbuzz/main", "--environment=prod"]
